from flask import Flask, Response, Request
import datetime, time
app = Flask(__name__)
 
@app.route("/")
def hello():
    return "Hello World!"

@app.route("/time")
def givetimeweb():
    def gibtime():
        while True:
            time.sleep(1)
            yield str(datetime.datetime.now())
    return Response(gibtime(),status=200, mimetype= "text/event-stream")

if __name__ == "__main__":
    app.run(port = 8080, threaded = True, debug=True)
